# HotFlip-CNN-pytorch
This hotflip attack code is based on https://github.com/srviest/char-cnn-text-classification-pytorch

**HotFlip** – algorithm for white-box adversarial attacks. In this project it is applied on character level. 
Implementation of two approaches:
1. [Greedy strategy](https://github.com/Fadeich/HotFlip-CNN-pytorch/blob/master/data_loader_hotflip_greedy.py)
2. [Beam search](https://github.com/Fadeich/HotFlip-CNN-pytorch/blob/master/data_loader_hotflip_beam.py)

Models for experiments can be downloaded from [storage](https://yadi.sk/d/3IyeSiPqk5b8XA):
1. [CharCNN](https://github.com/srviest/char-cnn-text-classification-pytorch/blob/master/model.py)
2. CharCNN adv – model with adversarial training for HotFlip attacks
3. CharCNN jac – model trained with [jacobian regularization](https://github.com/facebookresearch/jacobian_regularizer)

## Reference
* Xiang Zhang, Junbo Zhao, Yann LeCun. [Character-level Convolutional Networks for Text Classification](http://arxiv.org/abs/1509.01626). Advances in Neural Information Processing Systems 28 (NIPS 2015)
* Ebrahimi, Javid, Anyi Rao, Daniel Lowd and Dejing Dou. “HotFlip: White-Box Adversarial Examples for Text Classification.” ACL (2018).
* Chen, Laming, Guoxin Zhang and Eric Zhou. “Fast Greedy MAP Inference for Determinantal Point Process to Improve Recommendation Diversity.” NeurIPS (2018).
* Gao, Ji, Jack Lanchantin, Mary Lou Soffa and Yanjun Qi. “Black-Box Generation of Adversarial Text Sequences to Evade Deep Learning Classifiers.” 2018 IEEE Security and Privacy Workshops (SPW) (2018): 50-56.
* Judy Hoffman, Daniel A. Roberts, and Sho Yaida, "Robust Learning with Jacobian Regularization," 2019. [arxiv:1908.02729 [stat.ML]](https://arxiv.org/abs/1908.02729)
