import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#%matplotlib inline

df = pd.read_csv('data/ag_news_csv/test.csv', header=None)

from tqdm.autonotebook import tqdm
from model import CharCNN
import torch
from torch.autograd import Variable
from metric import print_f_score
import torch.nn.functional as F

import json
with open('alphabet.json') as f:
    res = json.load(f)
num_features = len(res)

class Args():
    num_features = num_features
    dropout = 0.5
    test_path = 'data/ag_news_csv/test.csv'
    alphabet_path = 'alphabet.json'
    batch_size = 20
    num_workers = 4
args = Args()

char_model = CharCNN(args)
checkpoint = torch.load('models/CharCNN_jac_best.pth.tar')
char_model.load_state_dict(checkpoint['state_dict'])
char_model = char_model.to("cuda:0")

def test_model(test_dataset, model, limit=None):
    corrects, avg_loss, accumulated_loss, size, fooled = 0, 0, 0, 0, 0
    predicates_all, target_all = [], []
    for i_batch, (data) in enumerate(tqdm(test_dataset)):
        inputs_init, inputs, target = data
        target.sub_(1)
        size+=1
        inputs_init = Variable(torch.unsqueeze(inputs_init, 0).to("cuda:0"))
        inputs = Variable(torch.unsqueeze(inputs, 0).to("cuda:0"))
        target = Variable(torch.unsqueeze(target, 0).to("cuda:0"))
        logit_init = model(inputs_init)
        logit = model(inputs)
        fooled += (logit_init.argmax() != logit.argmax()).detach().cpu().numpy()
    
        predicates = torch.max(logit, 1)[1].view(target.size()).data
        accumulated_loss += F.nll_loss(logit, target, size_average=False).data.item()
        corrects += (torch.max(logit, 1)[1].view(target.size()).data == target.data).sum().item()
        predicates_all+=predicates.cpu().numpy().tolist()
        target_all+=target.data.cpu().numpy().tolist()
        if limit is not None and i_batch == limit:
            break

    avg_loss = accumulated_loss/size
    accuracy = 100.0 * corrects/size
    fooled = 100.0 * fooled/size
    print('\rEvaluation - loss: {:.6f}  acc: {:.3f}%({}/{}) fooling rate: {:.3f}'.format(avg_loss, 
                                                                       accuracy, 
                                                                       corrects, 
                                                                       size,
                                                                       fooled))
    print_f_score(predicates_all, target_all)
    return accuracy, fooled, predicates_all, target_all
    
from data_loader_hotflip_greedy import AGNEWs_HotFlip_Greedy

test_dataset = AGNEWs_HotFlip_Greedy(per_corrupt=1, model=char_model, theta=0.5, dpp=False,
                                  label_data_path=args.test_path, alphabet_path=args.alphabet_path)
accuracy, fooled, predicates_all, target_all = test_model(test_dataset, char_model, limit=7600)

